package pl.airinfo.payment.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import pl.airinfo.service.model.Money;

@Builder
public class PaymentDTO {

    @JsonProperty
    private long customerPesel;
    @JsonProperty
    private int deviceSerialNumber;

    public long getCustomerPesel() {
        return customerPesel;
    }

    public int getDeviceSerialNumber() {
        return deviceSerialNumber;
    }
}
