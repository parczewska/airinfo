package pl.airinfo.payment.domain;

import org.springframework.stereotype.Service;
import pl.airinfo.common.ServiceType;
import pl.airinfo.payment.model.PaymentDTO;

import pl.airinfo.service.domain.ServiceRepository;

import pl.airinfo.service.model.Money;
import pl.airinfo.service.model.ServiceDAO;
import pl.airinfo.service.model.SubscriptionType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;
import java.util.Random;

@Service
public class PaymentService {

    private ServiceRepository serviceRepository;


    public PaymentService(ServiceRepository serviceRepository) {
        this.serviceRepository = serviceRepository;
    }

    public void calculatePrice(PaymentDTO paymentDTO) {

        long customerPesel = paymentDTO.getCustomerPesel();
        int deviceID = paymentDTO.getDeviceSerialNumber();

        Optional<ServiceDAO> result = serviceRepository.findByCustomerPeselAndDeviceSerialNumber(customerPesel, deviceID);

        if (result.isPresent()) {

            ServiceDAO serviceDAO = result.get();
            Random random = new Random();
            int numberBefore = random.nextInt(100000) + 1;
            float numberAfter = random.nextFloat();

            double amount = numberBefore + numberAfter;

            BigDecimal bigDecimal = new BigDecimal(amount);
            BigDecimal valueRound = bigDecimal.setScale(2, RoundingMode.HALF_DOWN);

            serviceDAO.setServicePrice(new Money(valueRound.toString()));
            serviceRepository.save(serviceDAO);
        }
    }

    public void randomSubscription(PaymentDTO paymentDTO) {

        long customerPesel = paymentDTO.getCustomerPesel();
        int deviceId = paymentDTO.getDeviceSerialNumber();
        ServiceType subscription = ServiceType.SUBSCRIPTION;

        Optional<ServiceDAO> result = serviceRepository.findByCustomerPeselAndDeviceSerialNumberAndServiceType(customerPesel, deviceId, subscription);

        if (result.isPresent()) {

            ServiceDAO serviceDAO = result.get();
            Random random = new Random();
            int numberBefore = random.nextInt(10000) + 1;
            float numberAfter = random.nextFloat();

            double amount = numberBefore + numberAfter;

            BigDecimal bigDecimal = new BigDecimal(amount);
            BigDecimal valueRound = bigDecimal.setScale(2, RoundingMode.HALF_DOWN);

            int randomSubscriptionType = new Random().nextInt(SubscriptionType.getSubscriptionValuesLength());
            SubscriptionType subscriptionType = SubscriptionType.values()[randomSubscriptionType];

            BigDecimal subscriptionPrice = valueRound.subtract(subscriptionType.gerFreeServiceThreshold());

            if (subscriptionPrice.doubleValue() < 0) {

                subscriptionPrice = new BigDecimal("0.00");
            }

            serviceDAO.setServicePrice(new Money(subscriptionPrice.toString()));
            serviceRepository.save(serviceDAO);
        }
    }
}
