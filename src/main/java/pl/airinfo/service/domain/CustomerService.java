package pl.airinfo.service.domain;

import org.springframework.stereotype.Service;
import pl.airinfo.service.error.InvalidCustomerDataException;
import pl.airinfo.service.model.CustomerDAO;
import pl.airinfo.service.model.CustomerDTO;
import pl.airinfo.service.model.CustomerMapping;

import java.util.Optional;

@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public CustomerDTO add(CustomerDTO customerDTO) {

        Optional<CustomerDAO> result = customerRepository.findByCustomerPesel(customerDTO.getCustomerPesel());

        if (result.isEmpty()) {

            CustomerDAO customerDAO = new CustomerDAO();
            customerDAO.setCustomerPesel(customerDTO.getCustomerPesel());
            customerDAO.setName(customerDTO.getName());
            customerDAO.setLastname(customerDTO.getLastname());
            customerDAO.setCity(customerDTO.getCity());
            customerDAO.setServiceType(customerDTO.getContract());
            customerRepository.save(customerDAO);
            return customerDTO;
        }
        throw new InvalidCustomerDataException("Istnieje klient o podanym numerze pesel.");
    }

    public CustomerDTO findByCustomerPesel(long customerPesel) {

        Optional<CustomerDAO> result = customerRepository.findByCustomerPesel(customerPesel);

        if (result.isPresent()) {
            return CustomerMapping.map(result.get());
        }
        throw new InvalidCustomerDataException("Nie istnieje użytkownik o podanym nr pesel.");
    }

    public void delete(long customerPesel) {

        Optional<CustomerDAO> result = customerRepository.findByCustomerPesel(customerPesel);

        if (result.isPresent()) {
            customerRepository.delete(result.get());
        } else {
            throw new InvalidCustomerDataException("Nie istnieje klient o podanym numerze pesel.");
        }
    }
}