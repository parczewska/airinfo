package pl.airinfo.service.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.airinfo.common.ServiceType;
import pl.airinfo.service.model.CustomerDAO;
import pl.airinfo.service.model.ServiceDAO;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface ServiceRepository extends JpaRepository<ServiceDAO, Long> {

    Optional<ServiceDAO> findByCustomerPeselAndDeviceSerialNumber(long customerPesel, int deviceSerialNumber);
    Collection<ServiceDAO> findAByServiceType(ServiceType serviceType);
    Optional<ServiceDAO> findByCustomerPeselAndDeviceSerialNumberAndServiceType(long customerPesel, int deviceSerialNumber, ServiceType serviceType);
}
