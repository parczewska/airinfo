package pl.airinfo.service.domain;

import org.springframework.stereotype.Service;
import pl.airinfo.report.model.ServiceDTO;
import pl.airinfo.service.error.InvalidCustomerDataException;
import pl.airinfo.common.ServiceType;
import pl.airinfo.service.model.CustomerDAO;
import pl.airinfo.service.model.ServiceDAO;

import java.util.Optional;

@Service
public class WarrantyService {

    private CustomerRepository customerRepository;
    private ServiceRepository serviceRepository;

    public WarrantyService(CustomerRepository customerRepository, ServiceRepository serviceRepository) {

        this.customerRepository = customerRepository;
        this.serviceRepository = serviceRepository;
    }

    public ServiceDTO order(WarrantyDTO warrantyDTO) {

        long customerPesel = warrantyDTO.getCustomerPesel();
        ServiceType warranty = ServiceType.WARRANTY;

        Optional<CustomerDAO> result = customerRepository.findByCustomerPeselAndServiceType(customerPesel, warranty);
        if (result.isPresent()) {

            ServiceDAO serviceDAO = new ServiceDAO();
            serviceDAO.warranty(warrantyDTO);
            serviceRepository.save(serviceDAO);

            return ServiceDTO.builder().reportingDate(serviceDAO.getReportingDate()).
                    executionDate(serviceDAO.getExecutionDate()).
                    servicePrice(serviceDAO.getServicePrice()).
                    serviceType(serviceDAO.getServiceType()).build();
        } else {
            throw new InvalidCustomerDataException("Nie istnieje klient o podanym numerze pesel z aktywną gwarancją.");
        }
    }

    public void delete(WarrantyDTO warrantyDTO) {

        long customerPesel = warrantyDTO.getCustomerPesel();
        int deviceId = warrantyDTO.getDeviceSerialNumber();
        ServiceType warranty = ServiceType.WARRANTY;

        Optional<ServiceDAO> result = serviceRepository.findByCustomerPeselAndDeviceSerialNumberAndServiceType(customerPesel, deviceId, warranty);
        if (result.isPresent()) {

            ServiceDAO serviceDAO = result.get();
            serviceRepository.delete(serviceDAO);
        } else {
            throw new InvalidCustomerDataException("Nie istnieje klient o podanym numerze pesel i id urządzenia.");
        }
    }
}
