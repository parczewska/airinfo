package pl.airinfo.service.domain;

import org.springframework.stereotype.Service;
import pl.airinfo.payment.domain.PaymentService;
import pl.airinfo.payment.model.PaymentDTO;
import pl.airinfo.report.model.ServiceDTO;
import pl.airinfo.service.error.InvalidCustomerDataException;
import pl.airinfo.common.ServiceType;
import pl.airinfo.service.model.CustomerDAO;
import pl.airinfo.service.model.ServiceDAO;
import pl.airinfo.service.model.SubscriptionDTO;

import java.util.Optional;

@Service
public class SubscriptionService {

    private CustomerRepository customerRepository;
    private ServiceRepository serviceRepository;
    private PaymentService paymentService;
    ;

    public SubscriptionService(CustomerRepository customerRepository, ServiceRepository serviceRepository, PaymentService paymentService) {

        this.customerRepository = customerRepository;
        this.serviceRepository = serviceRepository;
        this.paymentService = paymentService;
    }

    public ServiceDTO order(SubscriptionDTO subscriptionDTO) {

        long customerPesel = subscriptionDTO.getCustomerPesel();
        int deviceId = subscriptionDTO.getDeviceSerialNumber();
        ServiceType subscription = ServiceType.SUBSCRIPTION;

        Optional<CustomerDAO> result = customerRepository.findByCustomerPeselAndServiceType(customerPesel, subscription);

        if (result.isPresent()) {

            Optional<ServiceDAO> serviceResult = serviceRepository.findByCustomerPeselAndDeviceSerialNumberAndServiceType(customerPesel, deviceId, subscription);

            if (serviceResult.isEmpty()) {

                ServiceDAO serviceDAO = new ServiceDAO();
                serviceDAO.subscription(subscriptionDTO);
                serviceRepository.save(serviceDAO);

                PaymentDTO paymentDTO = PaymentDTO.builder().
                        customerPesel(customerPesel).deviceSerialNumber(serviceDAO.getDeviceSerialNumber()).build();
                paymentService.randomSubscription(paymentDTO);

                return ServiceDTO.builder().reportingDate(serviceDAO.getReportingDate()).
                        executionDate(serviceDAO.getExecutionDate()).
                        servicePrice(serviceDAO.getServicePrice()).
                        serviceType(serviceDAO.getServiceType()).build();

            } else {
                throw new InvalidCustomerDataException("Już zarejestrowano urządzenie na użytkownika o podanym numerze pesel.");
            }
        } else {
            throw new InvalidCustomerDataException("Nie istnieje klient o podanym numerze pesel z aktywnym abonamentem dla podanego urządzenia.");
        }
    }

    public void delete(SubscriptionDTO subscriptionDTO) {

        long customerPesel = subscriptionDTO.getCustomerPesel();
        int deviceId = subscriptionDTO.getDeviceSerialNumber();
        ServiceType subscription = ServiceType.SUBSCRIPTION;

        Optional<ServiceDAO> result = serviceRepository.findByCustomerPeselAndDeviceSerialNumberAndServiceType(customerPesel, deviceId, subscription);
        if (result.isPresent()) {

            ServiceDAO serviceDAO = result.get();
            serviceRepository.delete(serviceDAO);
        } else {
            throw new InvalidCustomerDataException("Nie istnieje klient o podanym numerze pesel z aktywnym abonamentem dla podanego urządzenia.");
        }
    }
}
