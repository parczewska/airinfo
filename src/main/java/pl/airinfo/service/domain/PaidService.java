package pl.airinfo.service.domain;

import org.springframework.stereotype.Service;
import pl.airinfo.common.ServiceType;
import pl.airinfo.payment.domain.PaymentService;
import pl.airinfo.payment.model.PaymentDTO;
import pl.airinfo.report.model.ServiceDTO;
import pl.airinfo.service.error.InvalidCustomerDataException;
import pl.airinfo.service.model.CustomerDAO;
import pl.airinfo.service.model.ServiceDAO;

import java.util.Optional;

@Service
public class PaidService {

    private CustomerRepository customerRepository;
    private ServiceRepository serviceRepository;
    private PaymentService paymentService;


    public PaidService(CustomerRepository customerRepository, ServiceRepository serviceRepository, PaymentService paymentService) {

        this.customerRepository = customerRepository;
        this.serviceRepository = serviceRepository;
        this.paymentService = paymentService;
    }

    public ServiceDTO order(PaidDTO paidDTO) {

        long customerPesel = paidDTO.getCustomerPesel();

        Optional<CustomerDAO> result = customerRepository.findByCustomerPesel(customerPesel);
        if (result.isPresent()) {

            ServiceDAO serviceDAO = new ServiceDAO();
            serviceDAO.paid(paidDTO);
            serviceRepository.save(serviceDAO);

            PaymentDTO paymentDTO = PaymentDTO.builder().
                    customerPesel(customerPesel).deviceSerialNumber(serviceDAO.getDeviceSerialNumber()).build();
            paymentService.calculatePrice(paymentDTO);

            return ServiceDTO.builder().reportingDate(serviceDAO.getReportingDate()).
                    executionDate(serviceDAO.getExecutionDate()).
                    servicePrice(serviceDAO.getServicePrice()).serviceType(serviceDAO.getServiceType()).build();

        } else {
            throw new InvalidCustomerDataException("W bazie nie istniej klient o podanym numerze pesel.");
        }
    }

    public void delete(PaidDTO paidDTO) {

        long customerPesel = paidDTO.getCustomerPesel();
        int deviceId = paidDTO.getDeviceSerialNumber();
        ServiceType paid = ServiceType.PAID;

        Optional<ServiceDAO> result = serviceRepository.findByCustomerPeselAndDeviceSerialNumberAndServiceType(customerPesel, deviceId, paid);

        if (result.isPresent()) {

            ServiceDAO serviceDAO = result.get();
            serviceRepository.delete(serviceDAO);
        } else {
            throw new InvalidCustomerDataException("W bazie nie istnieje klient o podanym numerze pesel i podanym numerze urządzenia.");
        }
    }
}
