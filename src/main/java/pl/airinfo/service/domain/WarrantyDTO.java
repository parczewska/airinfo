package pl.airinfo.service.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class WarrantyDTO {

    @JsonProperty
    private long customerPesel;
    @JsonProperty
    private int deviceSerialNumber;

    public long getCustomerPesel() {
        return customerPesel;
    }

    public int getDeviceSerialNumber() {
        return deviceSerialNumber;
    }
}
