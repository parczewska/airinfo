package pl.airinfo.service.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.airinfo.common.ServiceType;
import pl.airinfo.service.model.CustomerDAO;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerDAO, Long> {

    Optional<CustomerDAO> findByCustomerPeselAndServiceType(long customerPesel, ServiceType serviceType);
    Optional<CustomerDAO> findByCustomerPesel(long customerPesel);
}
