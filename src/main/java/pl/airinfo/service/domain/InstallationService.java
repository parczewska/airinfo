package pl.airinfo.service.domain;

import org.springframework.stereotype.Service;
import pl.airinfo.common.ServiceType;
import pl.airinfo.payment.domain.PaymentService;
import pl.airinfo.payment.model.PaymentDTO;
import pl.airinfo.report.model.ServiceDTO;
import pl.airinfo.service.error.InvalidCustomerDataException;
import pl.airinfo.service.model.CustomerDAO;
import pl.airinfo.service.model.InstallationDTO;
import pl.airinfo.service.model.ServiceDAO;

import java.util.Optional;

@Service
public class InstallationService {

    private CustomerRepository customerRepository;
    private ServiceRepository serviceRepository;
    private PaymentService paymentService;

    public InstallationService(CustomerRepository customerRepository, ServiceRepository serviceRepository, PaymentService paymentService) {

        this.customerRepository = customerRepository;
        this.serviceRepository = serviceRepository;
        this.paymentService = paymentService;
    }

    public ServiceDTO order(InstallationDTO installationDTO) {

        long customerPesel = installationDTO.getCustomerPesel();

        Optional<CustomerDAO> result = customerRepository.findByCustomerPesel(customerPesel);
        if (result.isPresent()) {

            ServiceDAO serviceDAO = new ServiceDAO();
            serviceDAO.installation(installationDTO);
            serviceRepository.save(serviceDAO);

            PaymentDTO paymentDTO = PaymentDTO.builder().
                    customerPesel(customerPesel).deviceSerialNumber(serviceDAO.getDeviceSerialNumber()).build();
            paymentService.calculatePrice(paymentDTO);

            CustomerDAO customerDAO = result.get();
            customerDAO.addDeviceSerialNumber(serviceDAO.getDeviceSerialNumber());
            customerRepository.save(customerDAO);

            return ServiceDTO.builder().reportingDate(serviceDAO.getReportingDate()).
                    executionDate(serviceDAO.getExecutionDate()).
                    servicePrice(serviceDAO.getServicePrice()).
                    serviceType(serviceDAO.getServiceType()).build();

        } else {
            throw new InvalidCustomerDataException(" Nie istnie klient o podanym numerze pesel, należy go zarejestrować.");
        }
    }

    public void delete(InstallationDTO installationDTO) {

        long customerPesel = installationDTO.getCustomerPesel();
        int deviceId = installationDTO.getDeviceSerialNumber();
        ServiceType installation = ServiceType.INSTALLATION;

        Optional<ServiceDAO> result = serviceRepository.findByCustomerPeselAndDeviceSerialNumberAndServiceType(customerPesel, deviceId, installation);

        if (result.isPresent()) {

            ServiceDAO serviceDAO = result.get();
            serviceRepository.delete(serviceDAO);
        } else {
            throw new InvalidCustomerDataException("Nie istnieje klient o podanym numerze pesel i id urządzenia.");
        }
    }
}
