package pl.airinfo.service.api;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.airinfo.report.model.ServiceDTO;
import pl.airinfo.service.domain.SubscriptionService;
import pl.airinfo.service.model.SubscriptionDTO;

@RestController
public class SubscriptionApi {

    private SubscriptionService subscriptionService;

    public SubscriptionApi(SubscriptionService subscriptionService) {

        this.subscriptionService = subscriptionService;
    }

    @RequestMapping(value = "/subscription", method = RequestMethod.POST)
    public ServiceDTO order(@RequestBody SubscriptionDTO subscriptionDTO) {
        return subscriptionService.order(subscriptionDTO);
    }

    @RequestMapping(value = "/subscription/cancel", method = RequestMethod.DELETE)
    public void delete(@RequestBody SubscriptionDTO subscriptionDTO) {
        subscriptionService.delete(subscriptionDTO);
    }
}
