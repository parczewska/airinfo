package pl.airinfo.service.api;

import org.springframework.web.bind.annotation.*;
import pl.airinfo.service.domain.CustomerService;
import pl.airinfo.service.model.CustomerDTO;

@RestController
public class CustomerApi {

    private CustomerService customerService;

    public CustomerApi(CustomerService customerService) {

        this.customerService = customerService;
    }

    @RequestMapping(value = "/customers", method = RequestMethod.POST)
    public CustomerDTO add(@RequestBody CustomerDTO customerDTO) {
        return customerService.add(customerDTO);
    }

    @RequestMapping(value = "customers/{customerPesel}", method = RequestMethod.GET)
    public CustomerDTO findByCustomerPesel(@PathVariable long customerPesel) {
        return customerService.findByCustomerPesel(customerPesel);
    }

    @RequestMapping(value = "/customers/{customerPesel}", method = RequestMethod.DELETE)
    public void delete(@PathVariable long customerPesel) {
        customerService.delete(customerPesel);
    }
}

