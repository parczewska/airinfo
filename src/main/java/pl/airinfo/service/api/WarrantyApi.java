package pl.airinfo.service.api;

import org.springframework.web.bind.annotation.*;
import pl.airinfo.report.model.ServiceDTO;
import pl.airinfo.service.domain.WarrantyDTO;
import pl.airinfo.service.domain.WarrantyService;

import java.util.Optional;

@RestController
public class WarrantyApi {

    private WarrantyService warrantyService;

    public WarrantyApi(WarrantyService warrantyService) {

        this.warrantyService = warrantyService;
    }

    @RequestMapping(value = "/warranty", method = RequestMethod.POST)
    public ServiceDTO order(@RequestBody WarrantyDTO warrantyDTO) {
        return warrantyService.order(warrantyDTO);
    }

    @RequestMapping(value = "/warranty/cancel", method = RequestMethod.DELETE)
    public void delete(@RequestBody WarrantyDTO warrantyDTO) {
        warrantyService.delete(warrantyDTO);

    }
}
