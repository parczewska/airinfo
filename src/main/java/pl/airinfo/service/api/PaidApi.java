package pl.airinfo.service.api;

import org.springframework.web.bind.annotation.*;
import pl.airinfo.report.model.ServiceDTO;
import pl.airinfo.service.domain.PaidDTO;
import pl.airinfo.service.domain.PaidService;
import pl.airinfo.service.model.CustomerDTO;

@RestController
public class PaidApi {

    private PaidService paidService;

    public PaidApi(PaidService paidService) {

        this.paidService = paidService;
    }

    @RequestMapping(value = "/paid", method = RequestMethod.POST)
    public ServiceDTO order(@RequestBody PaidDTO paidDTO) {
        return paidService.order(paidDTO);
    }

    @RequestMapping(value = "/paid/cancel", method = RequestMethod.DELETE)
    public void delete(@RequestBody PaidDTO paidDTO) {
        paidService.delete(paidDTO);
    }


}
