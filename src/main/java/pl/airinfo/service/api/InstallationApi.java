package pl.airinfo.service.api;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.airinfo.report.model.ServiceDTO;
import pl.airinfo.service.domain.InstallationService;
import pl.airinfo.service.model.InstallationDTO;

@RestController
public class InstallationApi {

    private InstallationService installationService;

    public InstallationApi(InstallationService installationService) {

        this.installationService = installationService;
    }

    @RequestMapping(value = "/installation", method = RequestMethod.POST)
    public ServiceDTO order(@RequestBody InstallationDTO installationDTO) {
        return installationService.order(installationDTO);
    }

    @RequestMapping(value = "installation/cancel", method = RequestMethod.DELETE)
    public void delete(@RequestBody InstallationDTO installationDTO) {
        installationService.delete(installationDTO);
    }
}
