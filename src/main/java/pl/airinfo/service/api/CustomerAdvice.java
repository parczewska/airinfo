package pl.airinfo.service.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.airinfo.service.error.InvalidCustomerDataException;

@RestControllerAdvice
public class CustomerAdvice {

    @ExceptionHandler(InvalidCustomerDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String InvalidCustomerDataAdvice(InvalidCustomerDataException invalidCustomerDataException) {

        return invalidCustomerDataException.getMessage();
    }
}
