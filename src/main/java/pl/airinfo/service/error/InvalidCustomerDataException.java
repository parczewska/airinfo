package pl.airinfo.service.error;

public class InvalidCustomerDataException extends RuntimeException{

    public InvalidCustomerDataException(String message) {
        super(message);
    }
}
