package pl.airinfo.service.model;

import pl.airinfo.common.ServiceType;
import pl.airinfo.service.domain.PaidDTO;
import pl.airinfo.service.domain.WarrantyDTO;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Random;

@Entity
@Table(name = "service")
public class ServiceDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "reporting_date")
    private String reportingDate;
    @Column(name = "execution_date")
    private String executionDate;
    @Column(name = "service_price")
    private double servicePrice;
    @Column(name = "customer_pesel")
    private long customerPesel;
    @Column(name = "device_serial_number")
    private int deviceSerialNumber;
    @Column(name = "contract")
    @Enumerated(EnumType.STRING)
    private ServiceType serviceType;

    public ServiceDAO() {
    }

    public void warranty(WarrantyDTO warrantyDTO) {

        reportingDate = LocalDateTime.now().toString();
        executionDate = LocalDateTime.now().plusDays(14).toString();
        servicePrice = 0;
        customerPesel = warrantyDTO.getCustomerPesel();
        deviceSerialNumber = warrantyDTO.getDeviceSerialNumber();
        serviceType = ServiceType.WARRANTY;
    }

    public void paid(PaidDTO paidDTO) {

        reportingDate = LocalDateTime.now().toString();
        executionDate = LocalDateTime.now().plusDays(14).toString();
        servicePrice = 0;
        customerPesel = paidDTO.getCustomerPesel();
        deviceSerialNumber = paidDTO.getDeviceSerialNumber();
        serviceType = ServiceType.PAID;
    }

    public void subscription(SubscriptionDTO subscriptionDTO) {

        reportingDate = LocalDateTime.now().toString();
        executionDate = LocalDateTime.now().plusDays(14).toString();
        servicePrice = 0;
        customerPesel = subscriptionDTO.getCustomerPesel();
        deviceSerialNumber = subscriptionDTO.getDeviceSerialNumber();
        serviceType = ServiceType.SUBSCRIPTION;
    }

    public void installation(InstallationDTO installationDTO) {

        reportingDate = LocalDateTime.now().toString();
        executionDate = LocalDateTime.now().plusDays(7).toString();
        servicePrice = 0;
        customerPesel = installationDTO.getCustomerPesel();
        deviceSerialNumber = installationDTO.getDeviceSerialNumber();
        serviceType = ServiceType.INSTALLATION;
    }

    public String getReportingDate() {
        return reportingDate;
    }

    public String getExecutionDate() {
        return reportingDate;
    }

    public double getServicePrice() {
        return servicePrice;
    }

    public long getCustomerPesel() {
        return customerPesel;
    }

    public int getDeviceSerialNumber() {
        return deviceSerialNumber;
    }

    public String getServiceType() {
        return serviceType.name();
    }

    public void setServicePrice(Money servicePrice) {
        this.servicePrice = servicePrice.asDouble();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServiceDAO that = (ServiceDAO) o;

        if (id != that.id) return false;
        if (customerPesel != that.customerPesel) return false;
        if (deviceSerialNumber != that.deviceSerialNumber) return false;
        return serviceType == that.serviceType;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (customerPesel ^ (customerPesel >>> 32));
        result = 31 * result + deviceSerialNumber;
        result = 31 * result + (serviceType != null ? serviceType.hashCode() : 0);
        return result;
    }
}
