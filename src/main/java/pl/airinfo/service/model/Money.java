package pl.airinfo.service.model;

import java.math.BigDecimal;

public class Money {

    private BigDecimal value;

    public Money(String value) {
        this.value = new BigDecimal(value);
    }

    public double asDouble() {
        return value.doubleValue();
    }

    public BigDecimal asBigDecimal() {
        return value;
    }
}
