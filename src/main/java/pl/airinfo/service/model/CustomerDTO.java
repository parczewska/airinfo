package pl.airinfo.service.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import pl.airinfo.common.ServiceType;

import java.util.List;

@Builder
public class CustomerDTO {

    @JsonProperty
    private long customerPesel;
    @JsonProperty
    private String name;
    @JsonProperty
    private String lastname;
    @JsonProperty
    private String city;
    @JsonProperty
    private ServiceType contract;


    public long getCustomerPesel() {
        return customerPesel;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getCity() {
        return city;
    }

    public ServiceType getContract() {
        return contract;
    }
}
