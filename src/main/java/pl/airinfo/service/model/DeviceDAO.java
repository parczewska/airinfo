package pl.airinfo.service.model;

import javax.persistence.*;
import java.util.Random;

@Entity(name = "devices")
@Table(name = "devices")
public class DeviceDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "device_serial_number")
    private long deviceSerialNumber;

    public DeviceDAO() {
    }

    public DeviceDAO(long deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }
}
