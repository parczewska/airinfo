package pl.airinfo.service.model;

import pl.airinfo.common.ServiceType;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "customer")
public class CustomerDAO {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    //private long id;
    @Column(name = "customer_pesel")
    private long customerPesel;
    @Column(name = "name")
    private String name;
    @Column(name = "lastname")
    private String lastname;
    @Column(name = "city")
    private String city;
    @Column(name = "service_type")
    @Enumerated(EnumType.STRING)
    private ServiceType serviceType;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_pesel")
    private List<DeviceDAO> devices = new LinkedList<>();

    public long getCustomerPesel() {
        return customerPesel;
    }

    public String getName(){
        return name;
    }

    public String getLastname(){
        return lastname;
    }

    public String getCity(){
        return city;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setCustomerPesel(long customerPesel) {
        this.customerPesel = customerPesel;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public void addDeviceSerialNumber(long deviceSerialNumber) {

        devices.add(new DeviceDAO(deviceSerialNumber));
    }
}
