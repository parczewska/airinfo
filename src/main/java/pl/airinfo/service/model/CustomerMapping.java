package pl.airinfo.service.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CustomerMapping {

    public static CustomerDTO map(CustomerDAO customerDAO) {

        return CustomerDTO.builder().
                customerPesel(customerDAO.getCustomerPesel()).name(customerDAO.getName()).
                lastname(customerDAO.getLastname()).city(customerDAO.getCity()).
                contract(customerDAO.getServiceType()).build();
    }
}
