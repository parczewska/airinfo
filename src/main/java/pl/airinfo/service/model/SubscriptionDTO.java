package pl.airinfo.service.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class SubscriptionDTO {

    @JsonProperty
    private long customerPesel;
    @JsonProperty
    private int deviceSerialNumber;

    public long getCustomerPesel() {
        return customerPesel;
    }

    public int getDeviceSerialNumber() {
        return deviceSerialNumber;
    }
}
