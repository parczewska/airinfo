package pl.airinfo.service.model;

import java.math.BigDecimal;

public enum SubscriptionType {
    ECO(new Money("29.99"), new Money("5000")),
    BASIC(new Money("49.99"), new Money("7500")),
    PREMIUM(new Money("99.99"), new Money("10000"));

    private Money subscriptionPrice;
    private Money freeServiceThreshold;

    SubscriptionType(Money subscriptionPrice, Money freeServiceThreshold) {
        this.subscriptionPrice = subscriptionPrice;
        this.freeServiceThreshold = freeServiceThreshold;
    }

    public static int getSubscriptionValuesLength() {

        return values().length;
    }

    public BigDecimal gerFreeServiceThreshold() {
        return freeServiceThreshold.asBigDecimal();

    }

}
