package pl.airinfo.report.api;

import org.springframework.web.bind.annotation.*;
import pl.airinfo.common.ServiceType;
import pl.airinfo.report.domain.ReportService;
import pl.airinfo.report.model.ReportDTO;

import java.util.List;

@RestController
public class ReportApi {

    private ReportService reportService;

    public ReportApi(ReportService reportService) {

        this.reportService = reportService;
    }

    @RequestMapping(value = "/report/{serviceType}", method = RequestMethod.GET)
    public ReportDTO generateReportByServiceType(@PathVariable String serviceType) {
        return reportService.generateReportByServiceType(serviceType);
    }

    @RequestMapping(value = "/report/all", method = RequestMethod.GET)
    public ReportDTO generateAllReports() {
        return reportService.generateAll();
    }

    @RequestMapping(value = "/reports", method = RequestMethod.GET)
    public List<ReportDTO> generateAllContractType() {
        return reportService.generateAllContractType();
    }


}
