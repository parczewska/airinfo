package pl.airinfo.report.model;

import pl.airinfo.service.model.ServiceDAO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ReportMapping {

    public static List<ServiceDTO> map(Collection<ServiceDAO> services) {

        List<ServiceDTO> servicesDTO = new ArrayList<>();

        for (ServiceDAO serviceDAO : services) {

            ServiceDTO serviceDTO = ServiceDTO.builder().
                    reportingDate(serviceDAO.getReportingDate()).
                    executionDate(serviceDAO.getExecutionDate()).
                    servicePrice(serviceDAO.getServicePrice()).
                    serviceType(serviceDAO.getServiceType()).build();

            servicesDTO.add(serviceDTO);
        }
        return servicesDTO;
    }
}
