package pl.airinfo.report.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.util.List;

@Builder
public class ReportDTO {

    @JsonProperty
    private int amountServices;
    @JsonManagedReference
    private List<ServiceDTO> services;

    public int getNumberServices() {
        if (services != null) {
            return services.size();
        }
        return 0;
    }

    @Override
    public String toString() {
        return "ReportDTO{" +
                "amountServices=" + amountServices +
                '}';
    }
}
