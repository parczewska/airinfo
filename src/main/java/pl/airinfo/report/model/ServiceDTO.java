package pl.airinfo.report.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class ServiceDTO {

    @JsonProperty
    private String reportingDate;
    @JsonProperty
    private String executionDate;
    @JsonProperty
    private double servicePrice;
    @JsonProperty
    private String serviceType;
}
