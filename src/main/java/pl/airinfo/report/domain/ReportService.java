package pl.airinfo.report.domain;

import org.springframework.stereotype.Service;
import pl.airinfo.common.ServiceType;
import pl.airinfo.service.domain.ServiceRepository;
import pl.airinfo.service.model.ServiceDAO;
import pl.airinfo.report.model.ReportDTO;
import pl.airinfo.report.model.ReportMapping;
import pl.airinfo.report.model.ServiceDTO;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

@Service
public class ReportService {

    private ServiceRepository serviceRepository;

    public ReportService(ServiceRepository serviceRepository) {
        this.serviceRepository = serviceRepository;
    }

    public ReportDTO generateReportByServiceType(String serviceType) {

        ServiceType type = ServiceType.valueOf(serviceType.toUpperCase());

        Collection<ServiceDAO> services = serviceRepository.findAByServiceType(type);

        List<ServiceDTO> servicesDTO = ReportMapping.map(services);

        return ReportDTO.builder().amountServices(services.size()).services(servicesDTO).build();
    }

    public ReportDTO generateAll() {

        Collection<ServiceDAO> services = serviceRepository.findAll();

        List<ServiceDTO> servicesDTO = ReportMapping.map(services);
        return ReportDTO.builder().amountServices(services.size()).services(servicesDTO).build();
    }

    public List<ReportDTO> generateAllContractType() {

        List<ReportDTO> reports = new LinkedList<>();
        ReportDTO paidReport = generateReportByServiceType(ServiceType.PAID.name());
        ReportDTO warrantyReport = generateReportByServiceType(ServiceType.WARRANTY.name());
        ReportDTO subscriptionReport = generateReportByServiceType(ServiceType.SUBSCRIPTION.name());
        ReportDTO installationReport = generateReportByServiceType(ServiceType.INSTALLATION.name());

        reports.add(paidReport);
        reports.add(warrantyReport);
        reports.add(subscriptionReport);
        reports.add(installationReport);

        return reports;
    }
}
