package pl.airinfo.common;

public enum ServiceType {

    WARRANTY,
    PAID,
    SUBSCRIPTION,
    INSTALLATION
}
