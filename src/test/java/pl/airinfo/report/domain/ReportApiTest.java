package pl.airinfo.report.domain;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.airinfo.common.ServiceType;
import pl.airinfo.payment.domain.PaymentService;
import pl.airinfo.report.api.ReportApi;
import pl.airinfo.report.model.ReportDTO;
import pl.airinfo.service.api.*;
import pl.airinfo.service.domain.*;
import pl.airinfo.service.model.CustomerDTO;
import pl.airinfo.service.model.InstallationDTO;
import pl.airinfo.service.model.SubscriptionDTO;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReportApiTest {

    private static CustomerRepository customerRepository = new CustomerRepositoryInMemory();
    private static ServiceRepository serviceRepository = new ServiceRepositoryInMemory();
    private static PaymentService paymentService = new PaymentService(serviceRepository);
    private static ReportApi reportApi = new ReportApi(new ReportService(serviceRepository));
    private static CustomerApi customerApi = new CustomerApi(new CustomerService(customerRepository));
    private static InstallationApi installationApi = new InstallationApi(new InstallationService(customerRepository, serviceRepository, paymentService));
    private static SubscriptionApi subscriptionApi = new SubscriptionApi(new SubscriptionService(customerRepository, serviceRepository, paymentService));
    private static WarrantyApi warrantyApi = new WarrantyApi(new WarrantyService(customerRepository, serviceRepository));
    private static PaidApi paidApi = new PaidApi(new PaidService(customerRepository, serviceRepository, paymentService));

    @BeforeAll
    static void init() {

        initCustomers();
        orderInstallation();
        orderSubscription();
        orderWarranty();
        orderPaid();
    }

    private static void initCustomers() {
        CustomerDTO customer1 = CustomerDTO.builder().customerPesel(123456789).name("Anna").
                lastname("Kowalska").city("Sopot").contract(ServiceType.SUBSCRIPTION).build();
        customerApi.add(customer1);

        CustomerDTO customer2 = CustomerDTO.builder().customerPesel(987654000).name("Jan").
                lastname("Zych").city("Warszawa").contract(ServiceType.PAID).build();
        customerApi.add(customer2);

        CustomerDTO customer3 = CustomerDTO.builder().customerPesel(123456123).name("Zofia").
                lastname("Kowalska").city("Warszawa").contract(ServiceType.INSTALLATION).build();
        customerApi.add(customer3);

        CustomerDTO customer4 = CustomerDTO.builder().customerPesel(988800321).name("Barbara").
                lastname("Nowak").city("Sopot").contract(ServiceType.WARRANTY).build();
        customerApi.add(customer4);
    }

    private static void orderInstallation() {
        InstallationDTO installationDTO = InstallationDTO.builder().customerPesel(123456123).deviceSerialNumber(1111).build();
        installationApi.order(installationDTO);
    }

    private static void orderSubscription() {
        SubscriptionDTO subscriptionDTO = SubscriptionDTO.builder().customerPesel(123456789).deviceSerialNumber(2222).build();
        subscriptionApi.order(subscriptionDTO);
    }

    private static void orderWarranty() {
        WarrantyDTO warrantyDTO = WarrantyDTO.builder().customerPesel(988800321).deviceSerialNumber(6666).build();
        warrantyApi.order(warrantyDTO);
    }

    private static void orderPaid() {
        PaidDTO paidDTO = PaidDTO.builder().customerPesel(987654000).deviceSerialNumber(5555).build();
        paidApi.order(paidDTO);
    }

    @Test
    void shouldGenerateReportByServiceTypeInstallation() {
        //given
        ReportDTO result = reportApi.generateReportByServiceType(ServiceType.INSTALLATION.name());
        //when
        int numberOfServicesExpected = 1;
        int actualNumberOfServices = result.getNumberServices();
        //then
        assertEquals(numberOfServicesExpected, actualNumberOfServices);
    }

    @Test
    void shouldGenerateReportByServiceTypeSubscription() {
        //given
        ReportDTO result = reportApi.generateReportByServiceType(ServiceType.SUBSCRIPTION.name());
        //when
        int numberOfServicesExpected = 1;
        int actualNumberOfServices = result.getNumberServices();
        //then
        assertEquals(numberOfServicesExpected, actualNumberOfServices);
    }

    @Test
    void shouldGenerateReportByServiceTypeWarranty() {
        //given
        ReportDTO result = reportApi.generateReportByServiceType(ServiceType.WARRANTY.name());
        //when
        int numberOfServicesExpected = 1;
        int actualNumberOfServices = result.getNumberServices();
        //then
        assertEquals(numberOfServicesExpected, actualNumberOfServices);
    }

    @Test
    void shouldGenerateReportByServiceTypePaid() {
        //given
        ReportDTO result = reportApi.generateReportByServiceType(ServiceType.PAID.name());
        //when
        int numberOfServicesExpected = 1;
        int actualNumberOfServices = result.getNumberServices();
        //then
        assertEquals(numberOfServicesExpected, actualNumberOfServices);
    }

    @Test
    void shouldGenerateAllServiceTypeReport() {
        //given
        ReportDTO result = reportApi.generateAllReports();
        //when
        int numberOfServicesExpected = 4;
        int actualNumberOfServices = result.getNumberServices();
        //then
        assertEquals(numberOfServicesExpected, actualNumberOfServices);
    }

    @Test
    void shouldGenerateAllContractTypeReport() {
        //given
        List<ReportDTO> result = reportApi.generateAllContractType();
        //when
        int numberOfReportsExpected = 4;
        int actualNumberOfReports = result.size();
        //then
        assertEquals(numberOfReportsExpected, actualNumberOfReports);
    }
}
