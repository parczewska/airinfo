package pl.airinfo.service.domain;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import pl.airinfo.common.ServiceType;
import pl.airinfo.service.model.CustomerDAO;

import java.util.*;
import java.util.function.Function;

public class CustomerRepositoryInMemory implements CustomerRepository {

    private Set<CustomerDAO> customers = new HashSet<>();

    @Override
    public Optional<CustomerDAO> findByCustomerPeselAndServiceType(long customerPesel, ServiceType serviceType) {
        return customers.stream().filter(e -> e.getCustomerPesel() == customerPesel).filter(e -> e.getServiceType().equals(serviceType)).findFirst();
    }

    @Override
    public Optional<CustomerDAO> findByCustomerPesel(long customerPesel) {
        return customers.stream().filter(e -> e.getCustomerPesel() == customerPesel).findFirst();
    }

    @Override
    public List<CustomerDAO> findAll() {
        return null;
    }

    @Override
    public List<CustomerDAO> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<CustomerDAO> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<CustomerDAO> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(CustomerDAO entity) {
        customers.remove(entity);


    }

    @Override
    public void deleteAllById(Iterable<? extends Long> longs) {

    }

    @Override
    public void deleteAll(Iterable<? extends CustomerDAO> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends CustomerDAO> S save(S entity) {
        if (customers.add(entity)) {
            return entity;
        }
        return null;
    }

    @Override
    public <S extends CustomerDAO> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<CustomerDAO> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends CustomerDAO> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public <S extends CustomerDAO> List<S> saveAllAndFlush(Iterable<S> entities) {
        return null;
    }

    @Override
    public void deleteAllInBatch(Iterable<CustomerDAO> entities) {

    }

    @Override
    public void deleteAllByIdInBatch(Iterable<Long> longs) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public CustomerDAO getOne(Long aLong) {
        return null;
    }

    @Override
    public CustomerDAO getById(Long aLong) {
        return null;
    }

    @Override
    public CustomerDAO getReferenceById(Long aLong) {
        return null;
    }

    @Override
    public <S extends CustomerDAO> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends CustomerDAO> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends CustomerDAO> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends CustomerDAO> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends CustomerDAO> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends CustomerDAO> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public <S extends CustomerDAO, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
        return null;
    }
}
