package pl.airinfo.service.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.airinfo.common.ServiceType;
import pl.airinfo.payment.domain.PaymentService;
import pl.airinfo.report.model.ServiceDTO;
import pl.airinfo.service.api.CustomerApi;
import pl.airinfo.service.api.InstallationApi;
import pl.airinfo.service.error.InvalidCustomerDataException;
import pl.airinfo.service.model.CustomerDTO;
import pl.airinfo.service.model.InstallationDTO;
import pl.airinfo.service.model.ServiceDAO;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class InstallationApiTest {

    private static CustomerRepository customerRepository = new CustomerRepositoryInMemory();
    private static ServiceRepository serviceRepository = new ServiceRepositoryInMemory();
    private static CustomerService customerService = new CustomerService(customerRepository);
    private static CustomerApi customerApi = new CustomerApi(customerService);
    private static InstallationService installationService = new InstallationService(customerRepository,
            serviceRepository, new PaymentService(serviceRepository));
    private static InstallationApi installationApi = new InstallationApi(installationService);

    @BeforeAll
    static void init() {
        CustomerDTO customer = CustomerDTO.builder().customerPesel(123456789).
                name("Anna").lastname("Kowalska").city("Warszawa").
                contract(ServiceType.INSTALLATION).build();
        customerApi.add(customer);
    }

    @Test
    void shouldOrderServiceInstallationIfCustomerExist() {
        //given
        InstallationDTO installationDTO = InstallationDTO.builder().customerPesel(123456789).
                deviceSerialNumber(99978).build();
        //when
        ServiceDTO serviceDTO = installationApi.order(installationDTO);
        //then
        Assertions.assertNotNull(serviceDTO);
    }

    @Test
    void shouldNotOrderServiceInstallationIfCustomerNotExist() {
        //given
        InstallationDTO installationDTO = InstallationDTO.builder().customerPesel(9898989).
                deviceSerialNumber(1212).build();
        //then
        assertThrows(InvalidCustomerDataException.class, () -> {
            installationApi.order(installationDTO);
        });
    }

    @Test
    void shouldDeleteServiceInstallationIfCustomerExist() {
        //given
        InstallationDTO installationDTO = InstallationDTO.builder().customerPesel(123456789).
                deviceSerialNumber(9978).build();
        installationApi.order(installationDTO);
        boolean serviceInstallationExist = true;
        Optional<ServiceDAO> customerResult = serviceRepository.
                findByCustomerPeselAndDeviceSerialNumberAndServiceType(123456789,
                        9978, ServiceType.INSTALLATION);
        assertEquals(serviceInstallationExist, customerResult.isPresent());
        //when
        installationApi.delete(installationDTO);
        Optional<ServiceDAO> result = serviceRepository.findByCustomerPeselAndDeviceSerialNumberAndServiceType(123456789,
                9978, ServiceType.INSTALLATION);
        boolean serviceInstallationNotExist = true;
        //then
        assertEquals(serviceInstallationNotExist, result.isEmpty());
    }

    @Test
    void shouldNotDeleteServiceInstallationIfCustomerNotExist() {
        //given
        InstallationDTO installationDTO = InstallationDTO.builder().customerPesel(987654321).
                deviceSerialNumber(1122).build();
        //then
        assertThrows(InvalidCustomerDataException.class, () -> {
            installationApi.delete(installationDTO);
        });
    }
}
