package pl.airinfo.service.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.airinfo.common.ServiceType;
import pl.airinfo.payment.domain.PaymentService;
import pl.airinfo.report.model.ServiceDTO;
import pl.airinfo.service.api.CustomerApi;
import pl.airinfo.service.api.InstallationApi;
import pl.airinfo.service.api.SubscriptionApi;
import pl.airinfo.service.error.InvalidCustomerDataException;
import pl.airinfo.service.model.CustomerDTO;
import pl.airinfo.service.model.ServiceDAO;
import pl.airinfo.service.model.SubscriptionDTO;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SubscriptionApiTest {

    private static CustomerRepository customerRepository = new CustomerRepositoryInMemory();
    private static ServiceRepository serviceRepository = new ServiceRepositoryInMemory();
    private static CustomerService customerService = new CustomerService(customerRepository);
    private static CustomerApi customerApi = new CustomerApi(customerService);
    private static PaymentService paymentService = new PaymentService(serviceRepository);
    private static SubscriptionService subscriptionService = new SubscriptionService(customerRepository, serviceRepository, paymentService);
    private static SubscriptionApi subscriptionApi = new SubscriptionApi(subscriptionService);

    @BeforeAll
    static void init() {
        CustomerDTO customer = CustomerDTO.builder().customerPesel(123456789).
                name("Anna").lastname("Kowalska").city("Warszawa").
                contract(ServiceType.SUBSCRIPTION).build();
        customerApi.add(customer);
    }

    @Test
    void shouldOrderSubscriptionServiceIfCustomerExist() {
        //given
        SubscriptionDTO subscriptionDTO = SubscriptionDTO.builder().customerPesel(123456789).
                deviceSerialNumber(1111).build();
        //when
        ServiceDTO serviceDTO = subscriptionApi.order(subscriptionDTO);
        //then
        Assertions.assertNotNull(subscriptionDTO);
    }

    @Test
    void shouldNotOrderSubscriptionServiceIfCustomerNotExist() {
        //given
        SubscriptionDTO subscriptionDTO = SubscriptionDTO.builder().customerPesel(117894321).
                deviceSerialNumber(3333).build();
        //then
        assertThrows(InvalidCustomerDataException.class, () -> {
            subscriptionApi.order(subscriptionDTO);
        });
    }

    @Test
    void shouldDeleteSubscriptionServiceIfCustomerExist() {
        //given
        SubscriptionDTO subscriptionDTO = SubscriptionDTO.builder().customerPesel(123456789).
                deviceSerialNumber(2222).build();
        subscriptionApi.order(subscriptionDTO);
        boolean serviceExist = true;
        Optional<ServiceDAO> customerResult = serviceRepository.
                findByCustomerPeselAndDeviceSerialNumberAndServiceType(123456789, 2222, ServiceType.SUBSCRIPTION);
        assertEquals(serviceExist, customerResult.isPresent());
        //when
        subscriptionApi.delete(subscriptionDTO);
        Optional<ServiceDAO> result = serviceRepository.
                findByCustomerPeselAndDeviceSerialNumberAndServiceType(123456789, 2222, ServiceType.SUBSCRIPTION);
        boolean serviceNotExist = true;
        //then
        assertEquals(serviceNotExist, result.isEmpty());
    }

    @Test
    void shouldNotDeleteSubscriptionServiceIfCustomerNotExist() {
        //given
        SubscriptionDTO subscriptionDTO = SubscriptionDTO.builder().customerPesel(190066321).
                deviceSerialNumber(9090).build();
        //then
        assertThrows(InvalidCustomerDataException.class, () -> {
            subscriptionApi.delete(subscriptionDTO);
        });
    }
}


