package pl.airinfo.service.domain;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import pl.airinfo.common.ServiceType;
import pl.airinfo.service.domain.ServiceRepository;
import pl.airinfo.service.model.ServiceDAO;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ServiceRepositoryInMemory implements ServiceRepository {

    private Set<ServiceDAO> services = new HashSet<>();

    @Override
    public Optional<ServiceDAO> findByCustomerPeselAndDeviceSerialNumber(long customerPesel, int deviceSerialNumber) {
        return services.stream().filter(e -> e.getCustomerPesel() == customerPesel).
                filter(e -> e.getDeviceSerialNumber() == deviceSerialNumber).findFirst();
    }

    @Override
    public Collection<ServiceDAO> findAByServiceType(ServiceType serviceType) {
        return services.stream().filter(e -> e.getServiceType().equals(serviceType.name())).collect(Collectors.toList());
    }

    @Override
    public Optional<ServiceDAO> findByCustomerPeselAndDeviceSerialNumberAndServiceType(long customerPesel, int deviceSerialNumber, ServiceType serviceType) {
        return services.stream().filter(e -> e.getCustomerPesel() == customerPesel).filter(e -> e.getDeviceSerialNumber() == deviceSerialNumber).
                filter(e -> e.getServiceType().equals(serviceType.name())).findFirst();
    }

    @Override
    public List<ServiceDAO> findAll() {
        return new ArrayList<>(services);
    }

    @Override
    public List<ServiceDAO> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<ServiceDAO> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<ServiceDAO> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(ServiceDAO entity) {
        services.remove(entity);

    }

    @Override
    public void deleteAllById(Iterable<? extends Long> longs) {

    }

    @Override
    public void deleteAll(Iterable<? extends ServiceDAO> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends ServiceDAO> S save(S entity) {

        if (services.add(entity)) {
            return entity;
        }
        return null;
    }

    @Override
    public <S extends ServiceDAO> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<ServiceDAO> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends ServiceDAO> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public <S extends ServiceDAO> List<S> saveAllAndFlush(Iterable<S> entities) {
        return null;
    }

    @Override
    public void deleteAllInBatch(Iterable<ServiceDAO> entities) {

    }

    @Override
    public void deleteAllByIdInBatch(Iterable<Long> longs) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public ServiceDAO getOne(Long aLong) {
        return null;
    }

    @Override
    public ServiceDAO getById(Long aLong) {
        return null;
    }

    @Override
    public ServiceDAO getReferenceById(Long aLong) {
        return null;
    }

    @Override
    public <S extends ServiceDAO> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends ServiceDAO> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends ServiceDAO> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends ServiceDAO> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends ServiceDAO> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends ServiceDAO> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public <S extends ServiceDAO, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
        return null;
    }
}
