package pl.airinfo.service.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.airinfo.common.ServiceType;
import pl.airinfo.payment.domain.PaymentService;
import pl.airinfo.report.model.ServiceDTO;
import pl.airinfo.service.api.PaidApi;
import pl.airinfo.service.error.InvalidCustomerDataException;
import pl.airinfo.service.model.CustomerDTO;
import pl.airinfo.service.model.ServiceDAO;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PaidApiTest {

    private static CustomerRepository customerRepository = new CustomerRepositoryInMemory();
    private static ServiceRepository serviceRepository = new ServiceRepositoryInMemory();
    private static CustomerService customerService = new CustomerService(customerRepository);
    private static PaymentService paymentService = new PaymentService(serviceRepository);
    private static PaidService paidService = new PaidService(customerRepository, serviceRepository, paymentService);
    private static PaidApi paidApi = new PaidApi(paidService);

    @BeforeAll
    static void init() {
        CustomerDTO customer = CustomerDTO.builder().customerPesel(123456789).
                name("Anna").lastname("Kowalska").city("Sopot").
                contract(ServiceType.PAID).build();
        customerService.add(customer);
    }

    @Test
    void shouldOrderPaidServiceIfCustomerExist() {
        //given
        PaidDTO paidDTO = PaidDTO.builder().customerPesel(123456789).
                deviceSerialNumber(9965).build();
        //when
        ServiceDTO serviceDTO = paidApi.order(paidDTO);
        //then
        Assertions.assertNotNull(serviceDTO);
    }

    @Test
    void shouldNotOrderPaidServiceIfCustomerNotExist() {
        //given
        PaidDTO paidDTO = PaidDTO.builder().customerPesel(987654321).
                deviceSerialNumber(3343).build();
        //then
        assertThrows(InvalidCustomerDataException.class, () -> {
            paidApi.order(paidDTO);
        });
    }

    @Test
    void shouldDeletePaidServiceIfCustomerExist() {
        //given
        PaidDTO paidDTO = PaidDTO.builder().customerPesel(123456789).
                deviceSerialNumber(4455).build();
        paidApi.order(paidDTO);
        boolean paidServiceExist = true;
        Optional<ServiceDAO> paidResult = serviceRepository.
                findByCustomerPeselAndDeviceSerialNumberAndServiceType(123456789, 4455, ServiceType.PAID);
        assertEquals(paidServiceExist, paidResult.isPresent());
        //when
        paidApi.delete(paidDTO);
        Optional<ServiceDAO> result = serviceRepository.
                findByCustomerPeselAndDeviceSerialNumberAndServiceType(123456789, 4455, ServiceType.PAID);
        boolean paidServiceNotExist = true;
        //then
        assertEquals(paidServiceNotExist, result.isEmpty());
    }

    @Test
    void shouldNotDeletePaidServiceIfCustomerNotExist() {
        //given
        PaidDTO paidDTO = PaidDTO.builder().customerPesel(987654329).
                deviceSerialNumber(1212).build();
        //then
        assertThrows(InvalidCustomerDataException.class, () -> {
            paidApi.delete(paidDTO);
        });
    }
}
