package pl.airinfo.service.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.airinfo.common.ServiceType;
import pl.airinfo.service.api.CustomerApi;
import pl.airinfo.service.error.InvalidCustomerDataException;
import pl.airinfo.service.model.CustomerDAO;
import pl.airinfo.service.model.CustomerDTO;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CustomerApiTest {

    private static CustomerRepository customerRepository = new CustomerRepositoryInMemory();
    private static CustomerService customerService = new CustomerService(customerRepository);
    private static CustomerApi customerApi = new CustomerApi(customerService);

    @BeforeAll
    static void init() {
        CustomerDTO newCustomer = CustomerDTO.builder().customerPesel(123456789).
                name("Anna").lastname("Nowak").city("Warszawa").
                contract(ServiceType.SUBSCRIPTION).build();
        customerApi.add(newCustomer);
    }

    @Test
    void shouldAddNewCustomer() {
        //then
        Assertions.assertDoesNotThrow(() -> {
            CustomerDTO result = customerApi.findByCustomerPesel(123456789);
        });
    }

    @Test
    void shouldNotAddCustomerWhoExist() {
        //when
        CustomerDTO customer = CustomerDTO.builder().customerPesel(123456789).
                name("Anna").lastname("Nowak").city("Warszawa").
                contract(ServiceType.SUBSCRIPTION).build();
        //then
        assertThrows(InvalidCustomerDataException.class, () -> {
            customerApi.add(customer);
        });
    }

    @Test
    void shouldFindByCustomerPeselIfCustomerExist() {
        //then
        Assertions.assertDoesNotThrow(() -> customerApi.findByCustomerPesel(123456789));
    }

    @Test
    void shouldNotFindByCustomerPeselIfCustomerNotExist() {
        //then
        Assertions.assertThrows(InvalidCustomerDataException.class, () -> {
            customerApi.findByCustomerPesel(120006009);
        });
    }

    @Test
    void shouldDeleteCustomer() {
        //when
        CustomerDTO customerResult = customerApi.findByCustomerPesel(123456789);
        Assertions.assertNotNull(customerResult);
        customerApi.delete(123456789);
        //then
        Assertions.assertThrows(InvalidCustomerDataException.class, () -> {
            CustomerDTO result = customerApi.findByCustomerPesel(123456789);
        });
    }

    @Test
    void shouldNotDeleteCustomerWhoNotExist() {
        assertThrows(InvalidCustomerDataException.class, () -> {
            customerApi.delete(232323232);
        });
    }
}
