package pl.airinfo.service.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.airinfo.common.ServiceType;
import pl.airinfo.report.model.ServiceDTO;
import pl.airinfo.service.api.CustomerApi;
import pl.airinfo.service.api.WarrantyApi;
import pl.airinfo.service.error.InvalidCustomerDataException;
import pl.airinfo.service.model.CustomerDTO;
import pl.airinfo.service.model.ServiceDAO;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class WarrantyApiTest {

    private static CustomerRepository customerRepository = new CustomerRepositoryInMemory();
    private static ServiceRepository serviceRepository = new ServiceRepositoryInMemory();
    private static CustomerService customerService = new CustomerService(customerRepository);
    private static WarrantyService warrantyService = new WarrantyService(customerRepository, serviceRepository);
    private static WarrantyApi warrantyApi = new WarrantyApi(warrantyService);
    private static CustomerApi customerApi = new CustomerApi(customerService);

    @BeforeAll
    static void init() {
        CustomerDTO customer = CustomerDTO.builder().customerPesel(987654321).
                name("Jan").lastname("Kowalski").city("Wrocław").
                contract(ServiceType.WARRANTY).build();
        customerApi.add(customer);
    }

    @Test
    void shouldOrderWarrantyServiceIfCustomerExist() {
        //given
        WarrantyDTO warrantyDTO = WarrantyDTO.builder().customerPesel(987654321).
                deviceSerialNumber(1111).build();
        //when
        ServiceDTO serviceDTO = warrantyApi.order(warrantyDTO);
        //then
        Assertions.assertNotNull(serviceDTO);
    }

    @Test
    void shouldNotOrderWarrantyServiceIfCustomerNotExist() {
        //given
        WarrantyDTO warrantyDTO = WarrantyDTO.builder().customerPesel(567894321).
                deviceSerialNumber(3333).build();
        //then
        assertThrows(InvalidCustomerDataException.class, () -> {
            warrantyApi.order(warrantyDTO);
        });
    }

    @Test
    void shouldDeleteWarrantyServiceIfCustomerExist() {
        //given
        WarrantyDTO warrantyDTO = WarrantyDTO.builder().customerPesel(987654321).
                deviceSerialNumber(1111).build();
        warrantyApi.order(warrantyDTO);
        boolean serviceExist = true;
        Optional<ServiceDAO> customerResult = serviceRepository.findByCustomerPeselAndDeviceSerialNumberAndServiceType(987654321, 1111, ServiceType.WARRANTY);
        assertEquals(serviceExist, customerResult.isPresent());
        //when
        warrantyApi.delete(warrantyDTO);
        Optional<ServiceDAO> result = serviceRepository.findByCustomerPeselAndDeviceSerialNumberAndServiceType(987654321, 1111, ServiceType.WARRANTY);
        boolean serviceNotExist = true;
        //then
        assertEquals(serviceNotExist, result.isEmpty());
    }

    @Test
    void shouldNotDeleteWarrantyServiceIfCustomerNotExist() {
        //given
        WarrantyDTO warrantyDTO = WarrantyDTO.builder().customerPesel(198765321).
                deviceSerialNumber(2233).build();
        //then
        assertThrows(InvalidCustomerDataException.class, () -> {
            warrantyApi.delete(warrantyDTO);
        });
    }
}
