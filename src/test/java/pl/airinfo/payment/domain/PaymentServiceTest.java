package pl.airinfo.payment.domain;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.airinfo.common.ServiceType;
import pl.airinfo.payment.model.PaymentDTO;
import pl.airinfo.service.domain.*;
import pl.airinfo.service.model.CustomerDTO;
import pl.airinfo.service.model.ServiceDAO;
import pl.airinfo.service.model.SubscriptionDTO;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PaymentServiceTest {

    private static CustomerRepository customerRepository = new CustomerRepositoryInMemory();
    private static ServiceRepository serviceRepository = new ServiceRepositoryInMemory();
    private static CustomerService customerService = new CustomerService(customerRepository);
    private static PaymentService paymentService = new PaymentService(serviceRepository);
    private static PaidService paidService = new PaidService(customerRepository, serviceRepository, paymentService);
    private static SubscriptionService subscriptionService = new SubscriptionService(customerRepository, serviceRepository, paymentService);


    @BeforeAll
    static void init() {
        CustomerDTO customer = CustomerDTO.builder().customerPesel(123443212).name("Anna").
                lastname("Nowak").city("Gdynia").contract(ServiceType.PAID).build();
        customerService.add(customer);
        PaidDTO paidDTO = PaidDTO.builder().customerPesel(123443212).deviceSerialNumber(1212).build();
        paidService.order(paidDTO);

        CustomerDTO customer1 = CustomerDTO.builder().customerPesel(888449999).name("Adam").
                lastname("Nowakowski").city("Sopot").contract(ServiceType.SUBSCRIPTION).build();
        customerService.add(customer1);
        SubscriptionDTO subscriptionDTO = SubscriptionDTO.builder().customerPesel(888449999).deviceSerialNumber(3322).build();
        subscriptionService.order(subscriptionDTO);
    }

    @Test
    void shouldCalculatePrice() {
        //given
        PaymentDTO paymentDTO = PaymentDTO.builder().customerPesel(123443212).deviceSerialNumber(1212).build();
        //when
        paymentService.calculatePrice(paymentDTO);
        Optional<ServiceDAO> result = serviceRepository.findByCustomerPeselAndDeviceSerialNumber(123443212, 1212);
        assertTrue(result.isPresent());
        ServiceDAO serviceDAO = result.get();
        double moreThan = 0;
        //then
        assertTrue(serviceDAO.getServicePrice() > moreThan);
    }

    @Test
    void shouldRandomSubscription() {
        //given
        PaymentDTO paymentDTO = PaymentDTO.builder().customerPesel(888449999).deviceSerialNumber(3322).build();
        //when
        paymentService.randomSubscription(paymentDTO);
        Optional<ServiceDAO> result = serviceRepository.findByCustomerPeselAndDeviceSerialNumberAndServiceType(888449999, 3322, ServiceType.SUBSCRIPTION);
        assertTrue(result.isPresent());
        ServiceDAO serviceDAO = result.get();
        double moreOrEquals = 0;
        //then
        assertTrue(serviceDAO.getServicePrice() >= moreOrEquals);
    }
}
